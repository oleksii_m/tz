﻿using UnityEngine;
using System.Collections;
using System;

public struct SpiritSetting
{
    public int StartCount;
    public int MaxCount;
    public float MinSpeed;
    public float MaxSpeed;
}
public class GameFieldModel : IGameFieldModel
{
    private SpiritSetting spiritSetting;
    private int spiritCount;
    private int point;

    public GameFieldModel(SpiritSetting setting)
    {
        spiritSetting = setting;
        spiritCount = spiritSetting.StartCount;
        point = 0;
    }

    public int Point
    {
        get { return point; }
    }

    public void AddSpirit()
    {
        spiritCount++;
    }

    public bool CanAddSpirit()
    {
        return spiritCount < spiritSetting.MaxCount;
    }

    public void DellSpirit()
    {
        spiritCount--;
        point++;
    }

    public SpiritSetting GetSpiritSetting()
    {
        return spiritSetting;
    }
}

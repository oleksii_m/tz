﻿using UnityEngine;
using System.Collections;
using System;

public interface IGameFieldModel
{
    SpiritSetting GetSpiritSetting();
    bool CanAddSpirit();
    void AddSpirit();
    void DellSpirit();

    int Point { get;}
}

public interface IGameFieldView
{
    void Initialized(SpiritSetting setting);
    void DestroySpirit(ISpiritView spirit);
    void AddSpirit();
    InputController GetInputController();
    void UpdatePoint(int point);


}

public interface ISpiritView
{
    void Initialized(float minSpeed, float maxSpeed, float maxHeight);
    Action<ISpiritView> OnClick { get; set; }
    GameObject GetGameObject();
}

public class GameFieldController
{
    private IGameFieldModel model;
    private IGameFieldView view;

    public GameFieldController(IGameFieldModel model, IGameFieldView view)
    {
        this.model = model;
        this.view = view;

        view.Initialized(model.GetSpiritSetting());
        view.GetInputController().OnClicked += SpiritClicked;
    }

    private void SpiritClicked(ISpiritView spirit)
    {
        model.DellSpirit();
        view.UpdatePoint(model.Point);
        view.DestroySpirit(spirit);
    }

    public void AddSpirit()
    {
        if (model.CanAddSpirit())
        {
            model.AddSpirit();
            view.AddSpirit();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GameFieldView : MonoBehaviour, IGameFieldView
{
    [SerializeField]
    private GameObject gameField;
    [SerializeField]
    private SpiritView spirit;
    [SerializeField]
    private Text spiritCount;
    [SerializeField]
    private Text pointCount;

    private InputController inputController;
    private SpiritSetting setting;
    private int spirits;
    public void Initialized(SpiritSetting setting)
    {
        this.setting = setting;
        DestroyAllChild(gameField);
        spirits = 0;
        inputController = new InputController();

        for (var index = 0; index < setting.StartCount; index++)
        {
            AddSpirit();
        }

        UpdatePoint(0);
    }

    private void DestroyAllChild(GameObject obj)
    {
        for (var i = 0; i < obj.transform.childCount; i++)
        {
            var child = obj.transform.GetChild(i).gameObject;
            Destroy(child);
        }
    }

    public void DestroySpirit(ISpiritView spirit)
    {
        inputController.DetatchSpiritView(spirit);

        spirits--;
        UpdateSpiritCount();

        Destroy(spirit.GetGameObject());
    }

    public InputController GetInputController()
    {
        return this.inputController;
    }

    private void UpdateSpiritCount()
    {
        spiritCount.text = string.Format("{0} / {1}", spirits, setting.MaxCount);
    }

    void SetRandomPosition(RectTransform current, RectTransform parent)
    {
        var x = parent.rect.size.x / 2 - current.rect.size.x / 2;
        var y = parent.rect.size.y / 2 - current.rect.size.y / 2;
        var xPos = UnityEngine.Random.Range(-x, x);

        current.position = new Vector3(xPos, -y, current.position.z);

    }

    public void UpdatePoint(int point)
    {
        pointCount.text = point.ToString();
    }

    public void AddSpirit()
    {
        spirits++;
        UpdateSpiritCount();

        var pref = (SpiritView)this.spirit;
        var spirit = Instantiate(pref);
        SetRandomPosition(spirit.GetComponent<RectTransform>(), gameField.GetComponent<RectTransform>());
        spirit.transform.SetParent(this.gameField.transform, false);

        spirit.Initialized(setting.MinSpeed, setting.MaxSpeed, -spirit.transform.localPosition.y);
        inputController.AttachSpiritView(spirit);
    }
}

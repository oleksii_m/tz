﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class SpiritView : MonoBehaviour, ISpiritView
{
    private Button _button;

    public Action<ISpiritView> OnClick
    {
        get;set;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void Initialized(float minSpeed, float maxSpeed, float maxHeight)
    {
        if (GetComponent<Button>() == null)
            gameObject.AddComponent<Button>();

        _button = GetComponent<Button>();
        _button.interactable = true;
        _button.onClick.AddListener(() =>
        {
            if (OnClick != null)
                OnClick(this);
        });

        var move = GetComponent<Move>();
        var speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
        move.Initialized(speed, maxHeight);
    }
}

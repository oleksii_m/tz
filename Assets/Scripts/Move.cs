﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    private float speed;
    private float maxHeight;

    public void Initialized(float speed, float maxHeight)
    {
        this.speed = speed;
        this.maxHeight = maxHeight;
    }

    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);

        var isOutOfFiel = transform.localPosition.y > maxHeight;

        if (isOutOfFiel)
            gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public interface IInputController
{
    event Action<ISpiritView> OnClicked;

    void AttachSpiritView(ISpiritView spirit);

    void DetatchSpiritView(ISpiritView spirit);

}

public class InputController : IInputController
{
    public event Action<ISpiritView> OnClicked;

    public InputController()
    {
        OnClicked = null;
    }

    public void AttachSpiritView(ISpiritView spirit)
    {
        spirit.OnClick += SpiritClecked;
    }


    public void DetatchSpiritView(ISpiritView spirit)
    {
        spirit.OnClick -= SpiritClecked;
    }

    private void SpiritClecked(ISpiritView spirit)
    {
        if (OnClicked != null)
            OnClicked(spirit);
    }
}

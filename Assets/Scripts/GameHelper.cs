﻿using UnityEngine;
using System.Collections;

public class GameHelper : MonoBehaviour
{
    [SerializeField, Header("Spirit")]
    private int startCount = 8;
    [SerializeField]
    private int maxCount = 10;
    [SerializeField]
    private float minSpeed = 10.0f;
    [SerializeField]
    private float maxSpeed = 100.0f;

    [SerializeField, Header("Other"), Space(10)]
    private GameFieldView fieldView;
    [SerializeField]
    private float addSpiritTime = 1.5f;

    private GameFieldController fieldController;

    public void ReStart()
    {
        OnDisable();
        OnEnable();
    }

    private void OnEnable()
    {
        var setting = new SpiritSetting()
        {
            MaxCount = maxCount,
            StartCount = startCount,
            MaxSpeed = maxSpeed,
            MinSpeed = minSpeed
        };

        var fieldModel = new GameFieldModel(setting);
        fieldController = new GameFieldController(fieldModel, fieldView);

        InvokeRepeating("AddSpirit", addSpiritTime, addSpiritTime);
    }

    private void OnDisable()
    {
        CancelInvoke("AddSpirit");
    }

    private void AddSpirit()
    {
        fieldController.AddSpirit();
    }
}
